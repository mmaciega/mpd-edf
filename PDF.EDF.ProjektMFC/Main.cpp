#include <afxwin.h>      //MFC core and standard components
#include <afxcmn.h>
#include <stdlib.h>

#include "resource.h"    //main symbols

#include <iostream>
#include "OknoGlowne.h"

/*
	Klasa reprezentuj�ca ca�� aplikacj�.
*/
class Aplikacja : public CWinApp
{
	public:
		Aplikacja() {  }		//konstruktor domy�lny

		/* Metoda inicjuj�ca okno aplikacji */
		virtual BOOL InitInstance()	
		{
		   CWinApp::InitInstance();
		   
		   //tworzymy g��wne okno dialogowe a nast�pnie je wy�wietlamy
		   OGlowne oknoGlowne;
		   m_pMainWnd = &oknoGlowne;
		   INT_PTR nResponse = oknoGlowne.DoModal();
		   return FALSE;

		}
};

//tworzymy obiekt aplikacji, w ten spos�b zostaje uruchomiony program
Aplikacja aplikacja;