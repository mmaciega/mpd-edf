//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OknoGlowne.rc
//
#define W_DODAJ_PROCES                  9
#define W_OKNO_GLOWNE                   101
#define L_LISTA_PROCESOW                1005
#define B_DODAJ_PROCES                  1006
#define B_USUN_PROCES                   1007
#define B_DODAJ_OK                      1008
#define B_USZEREGUJ                     1008
#define B_DODAJ_ANULUJ                  1009
#define E_DODAJ_CZAS_DZIALANIA          1010
#define E_DODAJ_OKRES                   1011
#define P_POLE_RYSOWANIA                1013
#define T_WARTOSC_U                     1014
#define T_LEGENDA_U                     1015
#define IDC_EDIT1                       1016
#define IDC_EDIT2                       1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
