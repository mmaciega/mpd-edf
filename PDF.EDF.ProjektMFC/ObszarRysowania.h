#pragma once
#ifndef OBSZAR_RYSOWANIA_H
#define OBSZAR_RYSOWANIA_H
#include <vector>
#include "Proces.h"

/*
	Definicja klasy, w kt�rej rysowany jest wykres uszeregowania.
*/
class CObszarRysowania : public CStatic
{
	DECLARE_DYNAMIC(CObszarRysowania)

public:
	CObszarRysowania();													//konstruktor domy�lny
	virtual ~CObszarRysowania();										//destruktor
	void ustawHistorie(std::vector< std::vector<int> > hSzeregowania);	//metoda odpowiedzialna za ustawienie nowej historii uszeregowania
	void ustawProcesy(std::vector<Proces> lProcesow);					//metoda odpowiedzialna za ustawienie listy proces�w uszeregowanych
private:
	std::vector< std::vector<int> > historiaSzeregowania;				//historia uszeregowania
	std::vector<Proces> listaProcesow;									//lista proces�w
	std::vector<COLORREF> listaKolorow;									//lista paru kolor�w, wykorzystywana podczas rysowania wykres�w

	/* ************************************
	*******STA�E DO MODYFIKOWANIA *********
	************************************ */
	const int ODL_POMIEDZY_WYKRESAMI;									//odleg�o�� pomi�dzy wykresami dla proces�w	
	const int ODL_WYKRESU_OD_ID;										//odleg�o�� wykresu od identyfikatora procesu
	const int ROZMIAR_BOKU_KWADRATU;									//rozmiar boku jednego kwadratu w wykresie
	const int CO_ILE_PODZIALKA;											//co ile jednostek ma by� wy�wietlona warto�� podzia�ki
		
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();												//metoda wywo�ywana podczas rysowania obiektu
};
#endif

