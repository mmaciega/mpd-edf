#include "ZarzadcaProcesami.h"
#include <limits>
#include <algorithm>
#include <iostream>

/* Konstruktor domy�lny. Ustawia warto�� maksymalnego czasu na 100 */
ZarzadcaProcesami::ZarzadcaProcesami() : MAKS_CZAS(100) {};

/* metoda odpowiedzialna za dodanie procesu do uszeregowania */
void ZarzadcaProcesami::dodajProces(Proces & proces) {
	listaProcesow.push_back(proces);
}

/* metoda odpowiedzialna za dodanie listy proces�w do uszeregowania */
void ZarzadcaProcesami::dodajProcesy(std::vector<Proces> & lProcesow) {
	for( int i = 0; i < lProcesow.size(); i++ )
	{
		ZarzadcaProcesami::dodajProces(lProcesow[i]);
	}
}

/* metoda odpowiedzialna za posortowanie proces�w wg kryterium najbli�szego wykonania */
void ZarzadcaProcesami::sortujProcesy() {
	std::sort(listaProcesow.begin(), listaProcesow.end(), porownaj);
}

/* metoda por�wnuj�ca dwa procesy */
bool ZarzadcaProcesami::porownaj(const Proces & p1, const Proces & p2) {
	if ((p1.dajOkres() - p1.dajCzasWykonania()) == (p2.dajOkres() - p2.dajCzasWykonania())) {
		return p1.dajID() > p2.dajID();
	} else {
		return (p1.dajOkres() - p1.dajCzasWykonania()) < (p2.dajOkres() - p2.dajCzasWykonania());
	}
}

/* metoda odpowiedzialna za wypisanie proces�w*/
void ZarzadcaProcesami::wypiszProcesy() {
	for( int i = 0; i < listaProcesow.size(); i++ )
	{
		std::cout << listaProcesow[ i ] << std::endl;
	}
}

/* metoda odpowiedzialna za wypisanie proces�w z listy przekazanej w argumencie */
void ZarzadcaProcesami::wypiszProcesy(const std::vector<Proces> & lProcesow) {
	for( int i = 0; i < lProcesow.size(); i++ )
	{
		std::cout << lProcesow[ i ] << std::endl;
	}
}

/* metoda odpowiedzialna za uszeregowanie proces�w zgodnie z algorytmem EDF	 */
void ZarzadcaProcesami::szeregujEDF() {
	//inicjujemy obiekt historii
	stworzHistorie();

	//co ka�d� jednostke czasu wykonujemy operacje
	for(int i = 0; i < MAKS_CZAS; i++) {
		try {
			//szukamy proces z najbli�szym zako�czeniem
			Proces procesAktualny = procesZPierwszymZakonczeniem(i);

			Proces* pProcesAktualny = NULL;
			
			//przelatujemy wszystkie procesy do uporz�dkowania
			for( int i = 0; i < listaProcesow.size(); i++ )
			{
				if (listaProcesow[i].dajID() == procesAktualny.dajID()) {
					pProcesAktualny = &listaProcesow[i];
					listaProcesow[i].wykonajPrace();	//proces z najbli�szym zako�czeniem pracuje
				} else {

					if (listaProcesow[i].dajStan() != Proces::PROCES_SKONCZONY) {
						listaProcesow[i].czekaj();	//pozosta�e procesy oczekuj� na swoj� kolej
					}

				}
			}

			//zapisujemy bie��cy stan do historii
			zapiszStanDoHistorii();

			//sprawdzamy czy aktualnie pracuj�cy proces zako�czy� ju� wszystkie zadania
			if (pProcesAktualny != NULL) {
				pProcesAktualny->sprawdzCzySkonczylPrace();
			}	

		} catch (int error) {
			//�aden proces nie pracuje
			//zapisujemy bie��cy stan do historii
			zapiszStanDoHistorii();
		}
	}

	//porz�dkujemy otrzyman� histori�
	uporzadkujHistorie();
}

/* metoda odpowiedzialna za stworzenie instacji dla obiektu historii uszeregowania */
void ZarzadcaProcesami::stworzHistorie() {
	for (int i = 0; i < listaProcesow.size(); i++) {
		std::vector<int> historiaProcesu;
		historiaSzeregowania.push_back(historiaProcesu);
	}		
}

/* metoda odpowiedzialna za zapisanie aktualnych stan�w proces� do historii */
void ZarzadcaProcesami::zapiszStanDoHistorii() {
	for (int i = 0; i < listaProcesow.size(); i++) {
		historiaSzeregowania[i].push_back(listaProcesow[i].dajStan());
	}		
}

/* metoda odpowiedzialna za uporz�dkowanie historii. Uporz�dkowanie polega na zamiane warto�ci odpowiadaj�cej stanowi PROCES_ZAKO�CZONY na warto�� 0 */
void ZarzadcaProcesami::uporzadkujHistorie() {
	//stara wersja
	/*for (int i = 0; i < listaProcesow.size(); i++) {
		std::vector<int>* historiaProcesu  = &historiaSzeregowania[i];

		bool czyZamieniono = false;
		for (int j = 0; j < historiaProcesu->size(); j++) {

			if (((*historiaProcesu)[j] == 3) && czyZamieniono == false ) {
				(*historiaProcesu)[j] = 2;
				czyZamieniono = true;
			} else if ((*historiaProcesu)[j] == 3) {
				(*historiaProcesu)[j] = 0;
			} else {
				czyZamieniono = false;
			}
		}
	}	*/

	//zamieniamy stan 3 (PROCES_ZAKONCZONY) na warto�� 0
	for (int i = 0; i < listaProcesow.size(); i++) {
		std::vector<int>* historiaProcesu  = &historiaSzeregowania[i];
		for (int j = 0; j < historiaProcesu->size(); j++) {
			if ((*historiaProcesu)[j] == 3) {
				(*historiaProcesu)[j] = 0;
			}
		}
	}
}

/* metoda odpowiedzialna za wy�wietlenie na standardowe wyj�cie historii uszeregowania */
void ZarzadcaProcesami::wyswietlHistorie() {
	for (int i = 0; i < listaProcesow.size(); i++) {
		std::cout << "p" << listaProcesow[i].dajID() << ": ";

		std::vector<int> historiaProcesu  = historiaSzeregowania[i];

		for (int j = 0; j < historiaProcesu.size(); j++) {
			std::cout << historiaProcesu[j];
		}
		std::cout << std::endl;
	}
}

/* metoda przygotowuj�ca zarz�dce do uszeregowania nowych proces�w */
void ZarzadcaProcesami::wyczysc() {
	listaProcesow.clear();
	historiaSzeregowania.clear();
}

/* metoda zwracaj�ca proces z najbli�szym zako�czeniem. Zwr�cony proces b�dzie pracowa�. */
Proces ZarzadcaProcesami::procesZPierwszymZakonczeniem(int aktualnyCzas) {
	//aktualizujemy wszystkie procesy, kt�rym rozpocz�� si� nowy okres
	std::vector<Proces> procesyZNowymiOkresami = aktualizacjaNowychOkresow(aktualnyCzas);

	//szukamy procesu pracuj�cego
	Proces* procesPracujacy = czyJakisProcesPracuje();

	int minCzasOczekiwania;
	if (procesPracujacy != NULL) {
		//mamy proces aktualnie pracuj�cy b�dziemy sprawdza� czy kt�rykolwiek proces z nowym okresem ma bli�szy czas wykonania
		if (procesyZNowymiOkresami.size() != 0) {
			Proces* procesMin = NULL;
			minCzasOczekiwania = std::numeric_limits<int>::max();
			for (int i = 0; i < procesyZNowymiOkresami.size(); i++) {
				int tymczasCzasOczekiwania = procesyZNowymiOkresami[i].dajOkres() - procesyZNowymiOkresami[i].dajCzasWykonania();

				if (tymczasCzasOczekiwania < minCzasOczekiwania) {
					minCzasOczekiwania = tymczasCzasOczekiwania;
					procesMin = &procesyZNowymiOkresami[i];
				}
			}	


			int aktPracCzasOczekiwania = (procesPracujacy->dajOkres() 
				- (procesPracujacy->dajDlugoscCzekania() + procesPracujacy->dajWykonanaPraca()))
				- (procesPracujacy->dajCzasWykonania() - procesPracujacy->dajWykonanaPraca());


			//if ((minCzasOczekiwania < aktPracCzasOczekiwania) 
					//|| ((minCzasOczekiwania == aktPracCzasOczekiwania) && (procesMin->dajID() < procesPracujacy->dajID()))) {
				
			if (minCzasOczekiwania < aktPracCzasOczekiwania) {
				procesPracujacy->uspijPrace();
				return *procesMin;
			} 
		}

		return *procesPracujacy;

	} else {
		//szukamy proces w�r�d u�pionych i do wykonania z najbli�szym czasem zako�czenia
		minCzasOczekiwania = std::numeric_limits<int>::max();
		int minIndeks = -1;
		for( int i = 0; i < listaProcesow.size(); i++ )
		{
			if (listaProcesow[i].dajStan() == Proces::PROCES_DO_WYKONANIA || listaProcesow[i].dajStan() == Proces::PROCES_USPIONY) {
				int tymczasCzasOczekiwania = (listaProcesow[i].dajOkres() 
				- (listaProcesow[i].dajDlugoscCzekania() + listaProcesow[i].dajWykonanaPraca()))
				- (listaProcesow[i].dajCzasWykonania() - listaProcesow[i].dajWykonanaPraca());
			
				if (tymczasCzasOczekiwania < minCzasOczekiwania) {
						minCzasOczekiwania = tymczasCzasOczekiwania;
						minIndeks = i;
				}
			}


		}

		if(minIndeks != -1) {
			return listaProcesow[minIndeks];
		} else {
			throw -1;
		}
	}
}

/* metoda zwracaj�ca warto�� NULL lub adres procesu aktualnie pracuj�cego */
Proces* ZarzadcaProcesami::czyJakisProcesPracuje() {
	for( int i = 0; i < listaProcesow.size(); i++ )
	{
		if (listaProcesow[i].dajStan() == Proces::PROCES_PRACUJE) {
			return &listaProcesow[i];
		}
	}

	return NULL;
}

/* metoda odpowiedzialna za zaaktualizowanie proces�w, kt�rym rozpocz�� si� nowy okres */
std::vector<Proces> ZarzadcaProcesami::aktualizacjaNowychOkresow(int aktualnyCzas) {
	std::vector<Proces> procesyZNowymiOkresami;

	if (aktualnyCzas != 0) {
		for( int i = 0; i < listaProcesow.size(); i++ )
		{
			if((aktualnyCzas % listaProcesow[i].dajOkres()) == 0) {
				listaProcesow[i].rozpocznijNowyOkres();
				procesyZNowymiOkresami.push_back(listaProcesow[i]);
			}
		}
	}

	return procesyZNowymiOkresami;
}

/* metoda zwracaj�ca wektor z histori� uszeregowania */
std::vector< std::vector<int> > ZarzadcaProcesami::dajHistoriaSzeregowania() {
	return historiaSzeregowania;
}
