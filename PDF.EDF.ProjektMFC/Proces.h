#ifndef PROCES_H
#define PROCES_H
#include <iostream>

/* 
Definicja klasy Proces. Obiekt klasy Proces reprezentuje jedno z zada�
do uszeregowania. 
*/
class Proces {
public:
	Proces(int czasWykonaniaT, int okresT); //konstruktor sparametryzowany

	//typ wyliczeniowy definiuj�cy stan, w jakim aktualnie znajduje si� dany proces
	enum StanyProcesu {
		  PROCES_SKONCZONY = 3,
		  PROCES_PRACUJE = 2,
		  PROCES_USPIONY = 1,
		  PROCES_DO_WYKONANIA = 0
	};

	int dajID() const;					//metoda zwracaj�ca ID procesu
	int dajOkres() const;				//metoda zwracaj�ca okres procesu
	int dajCzasWykonania() const;		//metoda zwracaj�ca czas wykonania procesu
	int dajPriorytet() const;			//metoda zwracaj�ca priorytet procesu
	int dajWykonanaPraca() const;		//metoda zwracaj�ca liczb� jednostek wykonanej ju� pracy w okresie procesu
	int dajDlugoscCzekania() const;		//metoda zwracaj�ca liczbe jednostek oczekiwania na wykonanie lub doko�czenie pracy procesu
	StanyProcesu dajStan() const;		//metoda zwracaj�ca aktualny stan procesu procesu

	void czekaj();						//metoda ustawiaj�ca proces w stan oczekiwania na wykonanie
	void wykonajPrace();				//metoda ustawiaj�ca proces w stan wykonywania zadania
	void sprawdzCzySkonczylPrace();		//metoda ustawiaj�ca proces w stan sko�czenia pracy je�li wszystkie zadania zosta�y wykonane
	void uspijPrace();					//metoda ustawiaj�ca w proces u�pienia aktualnie wykonywanego zadania
	void rozpocznijNowyOkres();			//metoda rozpoczynaj�ca nowy okres dla procesu

	friend std::ostream & operator<< (std::ostream &wyjscie, const Proces &p);	//metoda przeci��aj�ca operator << do wy�wietlenia informacji o obiekcie (referencja)
	friend std::ostream & operator<< (std::ostream &wyjscie, Proces* p);		//metoda przeci��aj�ca operator << do wy�wietlenia informacji o obiekcie (wska�nik)

private:
	int ID;								//identyfikator procesu
	int okres;							//okres procesu
	int czasWykonania;					//czas wykonania zadania procesu
	int priorytet;						//priorytet procesu
	int wykonanaPraca;					//liczba jednostek wykonanej ju� pracy procesu
	int dlugoscCzekania;				//liczba jednostek oczekiwania na wykonanie lub wznowienie pracy procesu
	StanyProcesu stan;					//stan procesu

	static int LICZNIK_PROCESOW;		//licznik wszystkich proces�w stworzony w programie
};
#endif