#include "stdafx.h"
#include "OknoDodajProces.h"
#include "afxdialogex.h"


// OknoDodajProces dialog

IMPLEMENT_DYNAMIC(OknoDodajProces, CDialog)

/* Konstruktor domy�lny */
OknoDodajProces::OknoDodajProces(CWnd* pParent /*=NULL*/)
	: CDialog(OknoDodajProces::IDD, pParent)
	, eCzasDzialaniaWart(_T(""))
	, eOkresWart(_T(""))
{

}

/* Destuktor */
OknoDodajProces::~OknoDodajProces()
{
}

void OknoDodajProces::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, E_DODAJ_CZAS_DZIALANIA, eCzasDzialaniaWart);
	DDX_Text(pDX, E_DODAJ_OKRES, eOkresWart);
}

//mapowanie odpowiedzi na komunikaty
BEGIN_MESSAGE_MAP(OknoDodajProces, CDialog)
	ON_BN_CLICKED(B_DODAJ_OK, &OknoDodajProces::OnBnClickedDodajOk)
	ON_BN_CLICKED(B_DODAJ_ANULUJ, &OknoDodajProces::OnBnClickedDodajAnuluj)
END_MESSAGE_MAP()


// OknoDodajProces message handlers

/* metoda wywo�ywana po klikni�ciu w przycisk OK */
void OknoDodajProces::OnBnClickedDodajOk()
{
	CDialog::OnOK();
}

/* metoda wywo�ywana po klikni�ciu w przycisk ANULUJ */
void OknoDodajProces::OnBnClickedDodajAnuluj()
{
	CDialog::OnCancel();
}
