#include <vector>
#include <iostream>
#include "Proces.h"

/* 
Definicja klasy ZarzadcaProcesami. Jest to obiekt, kt�rego zadaniem jest uszeregowanie proces�w zgodnie
z algorytmem EDF. Posiada metody odpowiedzialne, za odbieranie proces�w do uszeregowania, za uszeregowanie EDF
oraz wy�wietlenie historii uszeregowania.
*/
class ZarzadcaProcesami {
public:
	ZarzadcaProcesami();											//konstruktor domy�lny
	void dodajProces(Proces & proces);								//metoda odpowiedzialna za dodanie procesu do uszeregowania
	void dodajProcesy(std::vector<Proces> & lProcesow);				//metoda odpowiedzialna za dodanie listy proces�w do uszeregowania
	void wypiszProcesy();											//metoda odpowiedzialna za wypisanie proces�w
	void wypiszProcesy(const std::vector<Proces> & lProcesow);		//metoda odpowiedzialna za wypisanie proces�w z listy przekazanej w argumencie
	
	void sortujProcesy();											//metoda odpowiedzialna za posortowanie proces�w wg kryterium najbli�szego wykonania
	void szeregujEDF();												//metoda odpowiedzialna za uszeregowanie proces�w zgodnie z algorytmem EDF	
	void wyswietlHistorie();										//metoda odpowiedzialna za wy�wietlenie na standardowe wyj�cie historii uszeregowania

	std::vector< std::vector<int> > dajHistoriaSzeregowania();		//metoda zwracaj�ca wektor z histori� uszeregowania	

	void wyczysc();													//metoda przygotowuj�ca zarz�dce do uszeregowania nowych proces�w

private:
	std::vector<Proces> listaProcesow;								//lista proces�w do uszeregowania
	std::vector< std::vector<int> > historiaSzeregowania;			//historia uszeregowania proces�w
	const int MAKS_CZAS;											//maksymalny czas, do kt�rego zostanie wykonane uszeregowanie
	
	static bool porownaj(const Proces & p1, const Proces & p2);		//metoda por�wnuj�ca dwa procesy 

	void stworzHistorie();											//metoda odpowiedzialna za stworzenie instacji dla obiektu historii uszeregowania
	void zapiszStanDoHistorii();									//metoda odpowiedzialna za zapisanie aktualnych stan�w proces� do historii
	void uporzadkujHistorie();										//metoda odpowiedzialna za uporz�dkowanie historii. Uporz�dkowanie polega na zamiane warto�ci odpowiadaj�cej stanowi PROCES_ZAKO�CZONY na warto�� 0
	Proces procesZPierwszymZakonczeniem(int aktualnyCzas);			//metoda zwracaj�ca proces z najbli�szym zako�czeniem. Zwr�cony proces b�dzie pracowa�.
	Proces* czyJakisProcesPracuje();								//metoda zwracaj�ca warto�� NULL lub adres procesu aktualnie pracuj�cego
	std::vector<Proces> ZarzadcaProcesami::aktualizacjaNowychOkresow(int aktualnyCzas);	//metoda odpowiedzialna za zaaktualizowanie proces�w, kt�rym rozpocz�� si� nowy okres

	void wszystkieProcesyCzekaja();									//metoda wywo�ywana, gdy wszystkie procesy czekaj� na przydzielenie procesora
};