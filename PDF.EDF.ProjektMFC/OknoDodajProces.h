#pragma once
#ifndef OKNO_DODAJ_PROCES_H
#define OKNO_DODAJ_PROCES_H
#include "resource.h"
#include "afxwin.h"

/*
	Definicja klasy okna dialogowego dodaj proces. Za jego pomoc� mo�emy doda� proces do uszeregowania.
*/
class OknoDodajProces : public CDialog
{
	DECLARE_DYNAMIC(OknoDodajProces)

public:
	OknoDodajProces(CWnd* pParent = NULL);  //konstruktor domy�lne
	virtual ~OknoDodajProces();				//destruktor

	enum { IDD = W_DODAJ_PROCES };			//dane okna

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedDodajOk();				//metoda wywo�ywana po klikni�ciu w przycisk OK
	afx_msg void OnBnClickedDodajAnuluj();			//metoda wywo�ywana po klikni�ciu w przycisk ANULUJ
	CString eCzasDzialaniaWart;						//warto�� wprowadzona w pole z czasem dzia�ania procesu
	CString eOkresWart;								//warto�� wprowadzona w pole z okresem procesu
};
#endif