// ObszarRysowania.cpp : implementation file
//

#include "stdafx.h"
#include "ObszarRysowania.h"


// CObszarRysowania

IMPLEMENT_DYNAMIC(CObszarRysowania, CStatic)

/* Konstruktor domy�lny. Wprowadza pare podstawowych kolor� do listy kolor�w */
CObszarRysowania::CObszarRysowania() 
	: 
	ODL_POMIEDZY_WYKRESAMI(30), 
	ODL_WYKRESU_OD_ID(30), 
	ROZMIAR_BOKU_KWADRATU(10), 
	CO_ILE_PODZIALKA(5)
{
	listaKolorow.push_back(RGB(0, 0, 255));
	listaKolorow.push_back(RGB(255, 0, 0));
	listaKolorow.push_back(RGB(0, 255, 0));
	listaKolorow.push_back(RGB(255, 255, 0));
	listaKolorow.push_back(RGB(0, 255, 255));
}

/* Destruktor */
CObszarRysowania::~CObszarRysowania()
{
}

/* metoda odpowiedzialna za ustawienie nowej historii uszeregowania */
void CObszarRysowania::ustawHistorie(std::vector< std::vector<int> > hSzeregowania) {
	historiaSzeregowania.clear();
	historiaSzeregowania = hSzeregowania;
}

/* metoda odpowiedzialna za ustawienie listy proces�w uszeregowanych */
void CObszarRysowania::ustawProcesy(std::vector<Proces> lProcesow) {
	listaProcesow.clear();
	listaProcesow = lProcesow;
}


//mapowanie odpowiedzi na komunikaty
BEGIN_MESSAGE_MAP(CObszarRysowania, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CObszarRysowania message handlers

/* metoda wywo�ywana podczas rysowania obiektu. W niej odbywa si� proces rysowania wykres�w */
void CObszarRysowania::OnPaint()
{
	CPaintDC dc(this); 

    CRect rect;
    GetClientRect(&rect);
	dc.Rectangle(rect);	//wype�niamy ca�y obiekt kolorem bia�ym

	int szerokosc = rect.Width();

	//sprawdzamy czy mam histori� uszeregowania, je�li tak to nale�y narysowa� dla niej wykresy
	if (historiaSzeregowania.size() != 0) {
		int liczbaPodzialek;
		int x;
		int y;

		//rysujemy wykresy dla ka�dego z proces�w
		for (int i = 0; i < listaProcesow.size(); i++) {
			CString tekstProces;
			tekstProces.Format(L"p%d", (i+1));
			dc.TextOut( 10, 10 + (i*ODL_POMIEDZY_WYKRESAMI), tekstProces );	//wy�wietlamy identyfikator procesu

			std::vector<int> historiaProcesu  = historiaSzeregowania[i];
			CBrush brush(listaKolorow[i%listaKolorow.size()]);	//pobieramy jeden z kolor�w z naszej listy kolor�w

			liczbaPodzialek = 0;
			x = ODL_WYKRESU_OD_ID;
			y = 25 + (i*ODL_POMIEDZY_WYKRESAMI);


			for (int j = 0; j < historiaProcesu.size(); j++) {
				//je�li wyjdzie po za obszar zako�cz rysowanie
				if ( (x+ROZMIAR_BOKU_KWADRATU) > szerokosc ) {
					break;
				}

				//rysujemy kwadracik
				if (historiaProcesu[j] == 2) {
					dc.FillRect(CRect(x, y, x+ROZMIAR_BOKU_KWADRATU, y-10), &brush); 
				} else if (historiaProcesu[j] == 1) {
					dc.FillRect(CRect(x, y, x+ROZMIAR_BOKU_KWADRATU, y-2), &brush); 
				}

				x += ROZMIAR_BOKU_KWADRATU;
				++liczbaPodzialek;
			}

			//rysujemy podzia�ki
			x = ODL_WYKRESU_OD_ID;
			CPen penBlack(PS_SOLID, 0, RGB(0,0,0));
			dc.SelectObject(&penBlack);

			for (int j = 0; j < liczbaPodzialek; j++) {
				dc.MoveTo(x, y);

				if(j % listaProcesow[i].dajOkres() == 0) {
					dc.LineTo(x, y-15);
				} else {
					dc.LineTo(x, y-2);
				}

				dc.MoveTo(x, y);
				x += ROZMIAR_BOKU_KWADRATU;
				dc.LineTo(x, y);
			}
		}

		//rysujemy pozycje podzia�ek
		x = ODL_WYKRESU_OD_ID;
		y = 25 + (((listaProcesow.size()-1)*ODL_POMIEDZY_WYKRESAMI) + 5);
			
		CString tekstCzas;
		tekstCzas.Format(L"%d", 0);
		dc.TextOut( x, y , tekstCzas);

		for (int i = CO_ILE_PODZIALKA; i < liczbaPodzialek; i+=CO_ILE_PODZIALKA) {
			x += ROZMIAR_BOKU_KWADRATU * CO_ILE_PODZIALKA;

			tekstCzas.Format(L"%d", i);
			dc.TextOut( x - 7, y , tekstCzas);
		}
	}
}
