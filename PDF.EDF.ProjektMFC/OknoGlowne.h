#include <afxwin.h>
#include <afxcmn.h>
#include "resource.h"
#include "ZarzadcaProcesami.h"
#include "OknoDodajProces.h"
#include "ObszarRysowania.h"

class OGlowne : public CDialog
{
    public:
		OGlowne(CWnd* pParent = NULL);							//konstruktor domy�lny

		enum{IDD = W_OKNO_GLOWNE};

	private:
		CButton * pUszereguj;								//wska�nik na obiekt przycisku "Uszereguj"
		CListCtrl * pListaProcesow;							//wska�nik na obiekt listy proces�w
		CStatic * pWartoscU;								//wska�nik na obiekt tekstu wy�wietlaj�cy warto�� U
		CStatic * pLegendaU;								//wska�nik na obiekt tekstu wy�wietlaj�cy legend� dla warto�ci U
		CObszarRysowania obszarRysowania;					//obiekt, na kt�rym rysowane s� wykresy
						 
		ZarzadcaProcesami zarzadcaProcesami;				//obiekt odpowiedzialny za uszeregowanie proces�w

		bool czyTestPoprawny;

		/* ************************************
		*******STA�E DO MODYFIKOWANIA *********
		************************************ */
		const int MAKS_LICZBA_PROCESOW;						//maksymalna liczba proces�w do uszeregowania

		double policzWartoscU();							//metoda zwracaj�ca warto�� U (test uszeregowania) dla podanych proces�w, warto�� ta dla algorytmu EDF musi by� <= 1
		void aktualizujPrzyciskUszereguj();					//metoda odpowiedzialna za aktywacj� lub dezaktywacj� przycisku uszereguj, �eby dosz�o do aktywacji test uszeregowania musi zosta� spe�niony oraz liczba proces�w musi by� wi�ksza od zera

    protected:
		virtual void DoDataExchange(CDataExchange* pDX);
		virtual BOOL OnInitDialog();						//metoda inicjalizuj�ca okno dialogowe

	public:
		DECLARE_MESSAGE_MAP()
		afx_msg void OnBnClickedDodajProces();				//metoda wywo�ywana po klikn�ciu w przycisk "Dodaj proces"
		afx_msg void OnBnClickedUsunProces();				//metoda wywo�ywana po klikn�ciu w przycisk "Usu� proces"
		afx_msg void OnBnClickedUszereguj();				//metoda wyw�owyana po klikn�ciu w przycisk "Uszereguj"
		afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);	//metoda odpowiedzialna za ustawienie koloru dla warto�ci U, je�li test uszeregowania nie jest spe�niony warto�� zostanie wy�wietlona na czerwono, w przeciwnym wypadku na czarno
};