#include "Proces.h"
#include <iostream>

int Proces::LICZNIK_PROCESOW = 0;

/*
	Konstruktor sparametryzowany przyjmuj�cy dwa argumenty. Pierwszym jest czas wykonania zadania procesu,
	drugim okres procesu.
*/
Proces::Proces(int czasWykonaniaT, int okresT) 
	: okres(okresT), czasWykonania(czasWykonaniaT), ID(++LICZNIK_PROCESOW), stan(PROCES_DO_WYKONANIA),
	wykonanaPraca(0), dlugoscCzekania(0) {}

/* Metoda zwracaj�ca ID procesu */
int Proces::dajID() const {
	return ID;
}
 /* Metoda zwracaj�ca okres procesu */
int Proces::dajOkres() const {
	return okres;
}
 
/* Metoda zwracaj�ca czas wykonania procesu */
int Proces::dajCzasWykonania() const {
	return czasWykonania;
}

/* Metoda zwracaj�ca priorytet procesu */
int Proces::dajPriorytet() const {
	return priorytet;
}

/* Metoda zwracaj�ca liczb� jednostek wykonanej ju� pracy w okresie procesu */
int Proces::dajWykonanaPraca() const {
	return wykonanaPraca;
}

/* Metoda zwracaj�ca liczbe jednostek oczekiwania na wykonanie lub doko�czenie pracy procesu*/
int Proces::dajDlugoscCzekania() const {
	return dlugoscCzekania;
}

/* Metoda zwracaj�ca aktualny stan procesu procesu */
Proces::StanyProcesu Proces::dajStan() const {
	return stan;
}

/* Metoda ustawiaj�ca proces w stan wykonywania zadania */
void Proces::wykonajPrace() {
	if (stan != PROCES_PRACUJE) {
		stan = PROCES_PRACUJE;
	}

	wykonanaPraca++;

	/*if (wykonanaPraca == czasWykonania) {
		wykonanaPraca = 0;
		stan = PROCES_SKONCZONY;
	}*/
}
/* Metoda ustawiaj�ca proces w stan sko�czenia pracy je�li wszystkie zadania zosta�y wykonane */
void Proces::sprawdzCzySkonczylPrace() {
	if (wykonanaPraca == czasWykonania) {
		wykonanaPraca = 0;
		stan = PROCES_SKONCZONY;
	}
}

/* Metoda ustawiaj�ca w proces u�pienia aktualnie wykonywanego zadania */
void Proces::uspijPrace() {
	stan = PROCES_USPIONY;
}

/* Metoda ustawiaj�ca proces w stan oczekiwania na wykonanie */
void Proces::czekaj() {
	dlugoscCzekania++;
}

/* Metoda rozpoczynaj�ca nowy okres dla procesu */
void Proces::rozpocznijNowyOkres() {
	dlugoscCzekania = 0;
	wykonanaPraca = 0;
	stan = PROCES_DO_WYKONANIA;
}

/* Metoda przeci��aj�ca operator << do wy�wietlenia informacji o obiekcie (referencja) */
std::ostream & operator<< (std::ostream &wyjscie, const Proces &p) {
	return wyjscie << "Proces[" << p.ID << "]: " << " czas - " << p.czasWykonania << ", okres - " << p.okres;
}

/* Metoda przeci��aj�ca operator << do wy�wietlenia informacji o obiekcie (wska�nik) */
std::ostream & operator<< (std::ostream &wyjscie, Proces* p) {
	return wyjscie << *p;
}