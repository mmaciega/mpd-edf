#include "OknoGlowne.h"

OGlowne::OGlowne(CWnd* pParent) 
	: 
	CDialog(OGlowne::IDD, pParent), 
	MAKS_LICZBA_PROCESOW(7),
	czyTestPoprawny(false)
{    } //konstruktor domy�lny

void OGlowne::DoDataExchange(CDataExchange* pDX) {
			CDialog::DoDataExchange(pDX);
}

BOOL OGlowne::OnInitDialog() 
{ 
		CDialog::OnInitDialog();

		//przypisujemy wska�nikom elementy z naszego okna dialogowego
		pListaProcesow = (CListCtrl *) GetDlgItem(L_LISTA_PROCESOW);
		pUszereguj = (CButton *) GetDlgItem(B_USZEREGUJ);
		pLegendaU = (CStatic *) GetDlgItem(T_LEGENDA_U); 
		pWartoscU = (CStatic *) GetDlgItem(T_WARTOSC_U); 
		obszarRysowania.SubclassDlgItem(P_POLE_RYSOWANIA, this);

		pLegendaU->SetWindowText(L"");
		pWartoscU->SetWindowText(L"");
		pUszereguj->EnableWindow(false);	//wy��czamy przycisk Uszereguj

		pListaProcesow->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);	//ustawiamy list� w tryb reportu

		pListaProcesow->InsertColumn(0, _T("Czas dzia�ania"), LVCFMT_LEFT, 105);	//dodajemy kolumn� "Czas dzia�ania"
		pListaProcesow->InsertColumn(1, _T("Okres"), LVCFMT_LEFT, 105);				//dodajemy kolumn� "Okres"

		return true; 
}

/* metoda zwracaj�ca warto�� U (test uszeregowania) dla podanych proces�w, warto�� ta dla algorytmu EDF musi by� <= 1 */
double OGlowne::policzWartoscU() {
	double U = 0;

	for (int nItem = 0; nItem < pListaProcesow->GetItemCount(); nItem++)
	{
		CString czasDzialania = pListaProcesow->GetItemText(nItem, 0);
		CString okres = pListaProcesow->GetItemText(nItem, 1);

		U += (double) _wtoi(czasDzialania)/_wtoi(okres);
	}

	if (U <= 1) {
		czyTestPoprawny = true;
	} else {
		czyTestPoprawny = false;
	}

	return U;
}

/* metoda odpowiedzialna za aktywacj� lub dezaktywacj� przycisku uszereguj, �eby dosz�o do aktywacji test uszeregowania musi zosta� spe�niony oraz liczba proces�w musi by� wi�ksza od zera */
void OGlowne::aktualizujPrzyciskUszereguj() {
	if (czyTestPoprawny && pListaProcesow->GetItemCount()>0) {
		pUszereguj->EnableWindow(true);
	} else {
		pUszereguj->EnableWindow(false);
	}
}

//mapowanie odpowiedzi na komunikaty
BEGIN_MESSAGE_MAP(OGlowne, CDialog)
	ON_BN_CLICKED(B_DODAJ_PROCES, &OGlowne::OnBnClickedDodajProces)
	ON_BN_CLICKED(B_USUN_PROCES, &OGlowne::OnBnClickedUsunProces)
	ON_BN_CLICKED(B_USZEREGUJ, &OGlowne::OnBnClickedUszereguj)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

// OGlowne message handlers

/* metoda wywo�ywana po klikn�ciu w przycisk "Dodaj proces" */
void OGlowne::OnBnClickedDodajProces()
{
	//tworzymy obiekt z oknem dialogowym do dodania procesu oraz wy�wietlamy go
	OknoDodajProces oknoDodajProces;
	INT_PTR nRet = -1;
	nRet =  oknoDodajProces.DoModal();

	//odczytujemy warto�� zwr�con� przez okno dialogowe
   switch (nRet)
   {
      case -1: 
		 this->MessageBox(L"Okno dialogowe nie mog�o zosta� wy�wietlone!", L"B��d", MB_ICONERROR);
         break;
	  //klikni�to w przycisk OK
      case IDOK:
	  {
		CString czasDzialania = oknoDodajProces.eCzasDzialaniaWart;
		CString okres = oknoDodajProces.eOkresWart;

		//sprawdzamy czy podane warto�ci s� poprawne, je�li nie wy�wietlamy odpowiedni komunikat i ponownie otwieramy okno dialogowe
		if(czasDzialania == "" || okres == "" || czasDzialania == "0" || okres == "0") {
			this->MessageBox(L"Wprowadzono niepoprawne dane", L"B��d danych", MB_ICONERROR);
			OGlowne::OnBnClickedDodajProces();
		} else if (_wtoi(okres) < _wtoi(czasDzialania)) {
			this->MessageBox(L"Okres musi by� d�u�szy od czasu dzia�ania procesu", L"B��d danych", MB_ICONERROR);
			OGlowne::OnBnClickedDodajProces();
		} else {
			//sprawdzamy czy zosta�a dodana ju� maksymalna liczba proces�w
			if (pListaProcesow->GetItemCount() == MAKS_LICZBA_PROCESOW) {
				this->MessageBox(L"Dodano ju� maksymaln� liczb� proces�w", L"Maksymalna liczba proces�w", MB_ICONERROR);
			} else {
				//dodajemy nowy proces do listy proces�w
				int nIndex = pListaProcesow->InsertItem(pListaProcesow->GetItemCount(), czasDzialania);
				pListaProcesow->SetItemText(nIndex, 1, okres);
			
				//aktualizuje warto�� U
				CString wartoscU;
				wartoscU.Format(L"%lf <= 1", OGlowne::policzWartoscU()); 
				pLegendaU->SetWindowText(L"U = ");
				pWartoscU->SetWindowText(wartoscU);

				//aktualizujemy przycisk Uszereguj
				aktualizujPrzyciskUszereguj();
			}
		}

        break;
	  }
   };
}

/* metoda wywo�ywana po klikn�ciu w przycisk "Usu� proces" */
void OGlowne::OnBnClickedUsunProces()
{
	//sprawdzamy, kt�ry proces jest zaznaczono a nast�pnie go usuwamy
	for (int nItem = 0; nItem < pListaProcesow->GetItemCount(); )
	{
	  if (pListaProcesow->GetItemState(nItem, LVIS_SELECTED) == LVIS_SELECTED)
		pListaProcesow->DeleteItem(nItem);
	  else
		++nItem;
	}

	//sprawdzamy czy s� jakie� procesy na li�cie, je�li nie ma wy��czamy przycisk "Uszreguj"
	if (pListaProcesow->GetItemCount() == 0) {
		pLegendaU->SetWindowText(L"");
		pWartoscU->SetWindowText(L"");
	} else {
		//aktualizuje warto�� U
		CString wartoscU;
		wartoscU.Format(L"%lf <= 1", OGlowne::policzWartoscU()); 
		pWartoscU->SetWindowText(wartoscU);
	}

	//aktualizujemy przycisk Uszereguj
	aktualizujPrzyciskUszereguj();
}

/* metoda wywo�ywana po klikn�ciu w przycisk "Uszereguj" */
void OGlowne::OnBnClickedUszereguj()
{
	//przygotowujemy zarz�dce proces�w do nowego uszeregowania
	zarzadcaProcesami.wyczysc();

	//tworzymy list� wszystkich proces�w z listy
	std::vector<Proces> lProcesow;

	for (int nItem = 0; nItem < pListaProcesow->GetItemCount(); ++nItem)
	{
		CString czasDzialania = pListaProcesow->GetItemText(nItem, 0);
		CString okres = pListaProcesow->GetItemText(nItem, 1);

		lProcesow.push_back(Proces(_wtoi(czasDzialania), _wtoi(okres)));
	}

	//dodajemy list� proces�w do zarz�dzy, nast�pnie szeregujemy i zapisujemy histori� do zmiennej
	zarzadcaProcesami.dodajProcesy(lProcesow);
	zarzadcaProcesami.szeregujEDF();
	std::vector< std::vector<int> > historiaSzeregowania = zarzadcaProcesami.dajHistoriaSzeregowania();

	//przekazujemy histori� oraz list� proces�w do obiektu obszaru, kt�ry odpowiedzialny jest za rysowanie wykres�w 
	obszarRysowania.ustawHistorie(zarzadcaProcesami.dajHistoriaSzeregowania());
	obszarRysowania.ustawProcesy(lProcesow);

	//od�wie�amy okno z wykresami
	obszarRysowania.Invalidate();
}

/* metoda odpowiedzialna za ustawienie koloru dla warto�ci U, je�li test uszeregowania nie jest spe�niony warto�� zostanie wy�wietlona na czerwono, w przeciwnym wypadku na czarno */
HBRUSH OGlowne::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	//insteresuje nas jedynie kontrolka wy�wietlaj�ca warto�� U (test uszeregowania)
	if (pWnd->GetDlgCtrlID() == T_WARTOSC_U && czyTestPoprawny) {
		pDC->SetTextColor(RGB(0, 0, 0));    
	} else if (pWnd->GetDlgCtrlID() == T_WARTOSC_U && !czyTestPoprawny) {
		pDC->SetTextColor(RGB(255, 0, 0));
	}

	return hbr;
}
