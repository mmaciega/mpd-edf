# MPD EDF #

Program wykonuje szeregowanie zadań w systemach czasu rzeczywistego zgodnie z algorytmem
EDF. Poza zaimplementowaniem algorytmu przedstawia graficznie otrzymane uszeregowanie za pomocą wykresu Gantta.

## Algorytm EDF ##

Algorytm EDF jest dynamicznym algorytmem szeregowania używanym w systemach czasu rzeczywistego, który ustawia procesy w kolejce priorytetowej. Oznacza to, że priorytety przydzielane są dynamicznie w zależności od czasu wymaganego do zakończenia obliczeń. Zadanie, które musi się najszybciej wykonać otrzymuje najwyższy priorytet. W algorytmie tym dopuszcza się możliwość wywłaszczania bieżącego zadania, jeśli zadanie o wyższym 
priorytecie staje się gotowe.

## Wykorzystana technologia i środowisko ##

Aplikacja została stworzona w języku C++. Wykorzystano środowisko programistyczne Visual Studio Ultimate 2012. Do stworzenia interfejsu graficznego wykorzystano bibliotekę MFC (Microsoft Foundation Classes). Biblioteka MFC została napisana w C++ i jest obiektową oraz uproszczoną wersją Microsoft Windows API.



![Przechwytywanie.PNG](https://bitbucket.org/repo/EEpEKq/images/295213647-Przechwytywanie.PNG)
Uszeregowanie dla przykładowego zbioru zadań